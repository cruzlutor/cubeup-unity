﻿using UnityEngine;
using System.Collections;

public class TowerScroll : MonoBehaviour
{

    private Vector3 next = new Vector3(0, 0, 0);

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, next, (2 * Time.deltaTime));
    }

    public void Move(float addY)
    {
        next.y = transform.position.y - addY;
    }
}
