﻿using System.Collections;
using UnityEngine;

public class ColorManager
{

    public Color color;
    public string name;
    public int currentColor;
    public string[,] colors;

    // Use this for initialization
    public ColorManager()
    {
        currentColor = 1;
        colors = new string[4, 2] {
            { "red", "#FF2626" },
            { "blue", "#4DA6FF" },
            { "purple", "#B973FF" },
            { "yellow", "#FFD24D" }
        };
    }

    public void NextColor()
    {
        currentColor = (currentColor >= (colors.Length / 2) - 1 ? 0 : currentColor + 1);
        string[,] colorArray = new string[1, 2] { { colors[currentColor, 0], colors[currentColor, 1] } };
        name = colorArray[0, 0];
        ColorUtility.TryParseHtmlString(colorArray[0, 1], out color);
    }
}
