﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
//using Parse;

public class Lobby : MonoBehaviour {

	public Button button;
	public Text status;
	public bool isConnected = false;
	private Network network;
	private Cubeup cubeup;
	private Backend backend;

	void Awake(){
		backend = Backend.Instance;
		network = Network.Instance;
		cubeup = Cubeup.Instance;
		button.gameObject.SetActive (false);
	}

	void Start () {
		Connect ();
	}


	/**
	 * Connection functions
	 * 
	 **/

	// reload loby to reconnect
	public void Reconnect(){
		Application.LoadLevel ("Lobby");
	}

    // back to the menu
    public void Cancel(){
        network.Disconnect();
        Application.LoadLevel("Main");
    }

    // connect to photon
    void Connect(){
		PhotonNetwork.ConnectUsingSettings ("v1.0");
		StartCoroutine(WaitConnection());
	}

	// wait for connection or show reconnect button
	IEnumerator WaitConnection(){
		status.text = "connecting...";
		yield return new WaitForSeconds (5);
		if (isConnected == false) {
			button.gameObject.SetActive (true);
		}
	}

	// on connect to photon
	void OnConnectedToPhoton(){
		status.text = "looking game...";
		isConnected = true;
		button.gameObject.SetActive (false);
	}



	/**
	 * Rooms join functions
	 * 
	 **/
	void OnJoinedLobby (){
		JoinRoom ();
	}
	
	// join to room
	void JoinRoom(){
		PhotonNetwork.JoinRandomRoom ();
	}

	// on join random room fail
	void OnPhotonRandomJoinFailed(){
		string roomName = "" + Random.Range (000000, 999999) + "" + Time.time.ToString ();
		RoomOptions roomOptions = new RoomOptions() { isVisible = true, maxPlayers = network.maxPlayers };
		PhotonNetwork.CreateRoom(roomName, roomOptions, TypedLobby.Default);

	}
	
	// on success room connected
	void OnJoinedRoom(){
		OnPhotonPlayerConnected ();
	}

	// on external player connected
	void OnPhotonPlayerConnected(){
		UpdatePlayers ();
		CheckRoom ();
	}

	// check room players
	void CheckRoom(){
		if (PhotonNetwork.room.playerCount == network.maxPlayers) {
			PhotonNetwork.room.open = false;
			Application.LoadLevel("Game");
		}
	}
	
	/**
	 * Disconnections functions
	 * 
	 **/

	// on player disconnect
	void OnPhotonPlayerDisconnected(){
		UpdatePlayers ();
	}


	// update number connected players
	void  UpdatePlayers(){
		int players = PhotonNetwork.room.playerCount;
		status.text = "waiting players "+ players + "/" + network.maxPlayers;
	}
}
