﻿using UnityEngine;
using System.Collections.Generic;
using GoogleMobileAds.Api;

public class Cubeup : MonoBehaviour {

	public InterstitialAd loseAd;
	private Backend backend;
   // public string[,] colors;
	//private int currentColor = 1;


	// singleton
	private static Cubeup instance;
	public static Cubeup Instance{
		get {
			if(Cubeup.instance == null){
				instance =  GameObject.FindObjectOfType<Cubeup>();
				DontDestroyOnLoad(instance.gameObject);
				
			}
			return Cubeup.instance;
		}
	}

	// Use this for initialization
	void Awake() {
		if(instance == null){
			instance = this;
			DontDestroyOnLoad(this);
		}else{
			if(this != instance)
				Destroy(this.gameObject);
		}

		backend = Backend.Instance;
    }

	// Use this for initialization
	void Start () {
        /*
        colors = new string[4, 2] { 
            { "red", "#FF2626" },
            { "blue", "#4DA6FF" },         
            { "purple", "#B973FF" },
            { "yellow", "#FFD24D" }
        };
        */
	}

	public void RequestLoseAd(){
		if (Application.platform == RuntimePlatform.Android) {
			// load lose add
			loseAd = new InterstitialAd("ca-app-pub-6538434310321045/3263944014");
		}

		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			// load lose add
			loseAd = new InterstitialAd("ca-app-pub-6538434310321045/9031276016");
		}

		if (loseAd != null) {
			// Create an empty ad request.
			AdRequest request = new AdRequest.Builder ().Build ();
			// Load the interstitial with the request.
			loseAd.LoadAd (request);
		}
	}

	public void ShowLoaseAd(){
		if (loseAd != null) {
			if (loseAd.IsLoaded ()) {
				loseAd.Show ();
			}
		}
	}

    // Get different color
    /*
    public string[,] getColor()
    {
        currentColor = (currentColor >= (colors.Length/2)-1 ? 0 : currentColor + 1);
        string[,] color = new string[1, 2] { { colors[currentColor, 0], colors[currentColor, 1] } };
        return color;
    }
    */


    public bool ShowTutorial()
    {
        int tutorial = PlayerPrefs.GetInt("tutorial2", 0);
        PlayerPrefs.SetInt("tutorial2", 1);
        return (tutorial == 0);
    }
}
