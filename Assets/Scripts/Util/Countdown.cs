﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class Countdown : MonoBehaviour
{

    private static bool active;
    private static float currentTime;
    private static float waitTime;
    private static Text counter;
    private static UnityAction onFinish;


    void Awake()
    {
        active = false;
    }


    // Update is called once per frame
    void Update()
    {
        if (active == true)
        {

            if (currentTime >= 0)
            {
                currentTime -= Time.deltaTime;
                counter.text = "" + Mathf.Ceil(currentTime);
            }
            else
            {
                Finish();
            }
        }
    }

    public void Finish()
    {
        counter = null;
        waitTime = 0f;
        currentTime = 0f;
        active = false;
        onFinish();
    }

    public static void Init(float time, Text element, UnityAction cb)
    {
        if (active == false)
        {
            counter = element;
            active = true;
            waitTime = time;
            currentTime = time;
            onFinish = cb;
        }
    }
}
