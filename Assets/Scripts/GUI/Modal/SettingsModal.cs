﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class SettingsModal : MonoBehaviour {
	
	public Button button;
	public Toggle toggle;

	private static SettingsModal instance;
	
	public static SettingsModal Instance{
		get {
			if(SettingsModal.instance == null){
				instance =  GameObject.FindObjectOfType<SettingsModal>();
				DontDestroyOnLoad(instance.gameObject);
			}
			return SettingsModal.instance;
		}
	}
	
	void Awake() 
	{
		if(instance == null)
		{
			//If I am the first instance, make me the Singleton
			instance = this;
			instance.gameObject.SetActive(false);
			DontDestroyOnLoad(this);
			
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			instance.gameObject.SetActive(false);
			if(this != instance)
				Destroy(this.gameObject);
		}
		button.onClick.AddListener (new UnityAction(Hide));
		//game.GetComponent<Multiplayer> ().playing = false;
	}
	
	public void Show(){
		gameObject.SetActive (true);
		int soundEnabled = PlayerPrefs.GetInt ("sound", 1);
		toggle.isOn = (soundEnabled == 1);
	}

	public void OnChange(){
		int value = 0;
		AudioListener.volume = 0;
		if (toggle.isOn) {
			value = 1;
			AudioListener.volume = 1;
		}
		PlayerPrefs.SetInt ("sound", value);
	}
	
	void Hide(){
		gameObject.SetActive (false);
	}
}