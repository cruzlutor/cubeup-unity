﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class DialogModal : MonoBehaviour {

	public Text title;
	public Text message;
	public Button button;

	private static DialogModal instance;

	public static DialogModal Instance{
		get {
			if(DialogModal.instance == null){
				instance =  GameObject.FindObjectOfType<DialogModal>();
				DontDestroyOnLoad(instance.gameObject);
			}
			return DialogModal.instance;
		}
	}

	void Awake() 
	{
		if(instance == null)
		{
			//If I am the first instance, make me the Singleton
			instance = this;
			instance.gameObject.SetActive(false);
			DontDestroyOnLoad(this);

		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			instance.gameObject.SetActive(false);
			if(this != instance)
				Destroy(this.gameObject);
		}

		//game.GetComponent<Multiplayer> ().playing = false;
	}

	public void Show(string _title, string _message, UnityAction onClose){
		gameObject.SetActive (true);
		title.text = _title;
		message.text = _message;

		button.onClick.RemoveAllListeners ();
		button.onClick.AddListener (new UnityAction(Hide));
		button.onClick.AddListener (onClose);

		GameObject game = GameObject.FindGameObjectWithTag ("Game");
		if (game) {
			//game.GetComponent<Multiplayer> ().playing = false;
		}
	}

	void Hide(){
		gameObject.SetActive (false);
	}
}
