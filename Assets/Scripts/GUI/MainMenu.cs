﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	public GameObject error;
	public string status;
	public GameObject loading;
	public GameObject guestMenu;
	public GameObject userMenu;
	public GameObject nicknameMenu;
	public GameObject reconnectMenu;
	public GameObject googleButton;
	public GameObject facebookButton;
	public GameObject iosButton;

	void Awake(){
		status = "loader";
		error.SetActive (false);
		guestMenu.SetActive (false);
		userMenu.SetActive (false);
		loading.SetActive (false);
		nicknameMenu.SetActive (false);
		reconnectMenu.SetActive (false);

		if (Application.platform == RuntimePlatform.Android) {
			googleButton.SetActive(true);
			facebookButton.SetActive(false);
			iosButton.SetActive(false);

		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
			googleButton.SetActive(false);
			facebookButton.SetActive(false);
			iosButton.SetActive(true);	

		} else if (Application.platform == RuntimePlatform.WebGLPlayer) {
			googleButton.SetActive(false);
			facebookButton.SetActive(true);
			iosButton.SetActive(false);
			
		} else if (Application.platform == RuntimePlatform.WindowsEditor) {
			googleButton.SetActive(false);
			facebookButton.SetActive(true);
			iosButton.SetActive(false);
			
		} else {
			googleButton.SetActive(false);
			facebookButton.SetActive(true);
			iosButton.SetActive(false);
		}
	}

	public void GuestMenu(){
		status = "guest";
		guestMenu.SetActive (true);
		userMenu.SetActive (false);
		loading.SetActive (false);
		nicknameMenu.SetActive (false);
		reconnectMenu.SetActive (false);
	}
	
	public void UserMenu(){
		status = "user";
		guestMenu.SetActive (false);
		userMenu.SetActive (true);
		nicknameMenu.SetActive (false);
		loading.SetActive (false);
	}

	public void Loader(){
		status = "loader";
		guestMenu.SetActive (false);
		userMenu.SetActive (false);
		loading.SetActive (true);
		nicknameMenu.SetActive (false);
		reconnectMenu.SetActive (false);
	}

	public void Nickname(){
		status = "nickname";
		guestMenu.SetActive (false);
		userMenu.SetActive (false);
		loading.SetActive (false);
		nicknameMenu.SetActive (true);
		reconnectMenu.SetActive (false);
	}
	
	public void Reconnect(){
		status = "reconnect";
		guestMenu.SetActive (false);
		userMenu.SetActive (false);
		loading.SetActive (false);
		nicknameMenu.SetActive (false);
		reconnectMenu.SetActive (true);
	}
}