﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerRank : MonoBehaviour {

	private Cubeup cubeup;
	public Text rank;
	public Text username;
	public Text score;

	void Awake(){
		cubeup = Cubeup.Instance;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetScore(int type,  int _rank, string _username, int _score){
		Color color;
		string hexa = "#BBBBBB";

		if (_rank == 1) {
			hexa = "#FFCD00";
		}
	
		if (_rank == 2) {
			hexa = "#818181";
		}

		if (_rank == 3) {
			hexa = "#B28C48";
		}
        
		if (ColorUtility.TryParseHtmlString(hexa, out color)) {
			rank.color = color;
		}
        

		rank.text = "#"+_rank;
		username.text = _username;
		if (type == 2) {
			//score.text = "" + Mathf.Floor (cubeup.CalculateLevel (_score));
		} else {
			score.text = "" + _score;
		}

	}
}
