﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
//using System.Threading.Tasks;
//using Parse;

public class Leaderboard : MonoBehaviour {
	
	public Button levelButton;
	public Button scoreButton;
	public Button accurateButton;
	public AudioSource tapSound;
	public Button button;
	public GameObject players;
	public GameObject loading;
	private int type;
	private Backend backend;
	private GameObject playerPrefab;
	private bool busy;


	// singleton
	private static Leaderboard instance;
	public static Leaderboard Instance{
		get {
			if(Leaderboard.instance == null){
				instance =  GameObject.FindObjectOfType<Leaderboard>();
				DontDestroyOnLoad(instance.gameObject);
			}
			return Leaderboard.instance;
		}
	}

	// Use this for initialization
	void Awake () {
		if(instance == null){
			instance = this;
			DontDestroyOnLoad(this);
		}else{
			instance.gameObject.SetActive(false);
			if(this != instance)
				Destroy(this.gameObject);
		}
		playerPrefab = Resources.Load ("Prefabs/PlayerRank") as GameObject;
		backend = Backend.Instance;

	}

	// Use this for initialization
	void Start () {
		button.onClick.AddListener (new UnityAction (Hide));
		//gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowScore(){
		Show (1);
	}

	public void ShowLevel(){
		Show (2);
	}

	public void ShowAccurate(){
		Show (3);
	}

	public void Show(int _type){
		gameObject.SetActive (true);
		if (busy == false) {
			type = _type;
			busy = true;
			Clear ();
			SelectButton ();
			loading.SetActive (true);
			LoadPlayers ();
		}
	}

	void SelectButton(){
        
		Color blue;
        ColorUtility.TryParseHtmlString("#46C2FF", out blue);
		
		Color grey;
        ColorUtility.TryParseHtmlString("#AAAAAA", out grey);

		levelButton.GetComponent<Text> ().color = grey;
		scoreButton.GetComponent<Text> ().color = grey;
		accurateButton.GetComponent<Text> ().color = grey;

		if (type == 1)
			scoreButton.GetComponent<Text>().color = blue;

		if (type == 2)
			levelButton.GetComponent<Text>().color = blue;

		if (type == 3)
			accurateButton.GetComponent<Text>().color = blue;

    
	}

	public void LoadPlayers(){
		backend.GetLeaders (type, OnLoadLeaders);
	}

	void OnLoadLeaders(bool success, string result){
		busy = false;

		if (!success) {
			DialogModal.Instance.Show ("CONNECTION ERROR", "sorry we lost you, check your internet connection and try again", new UnityAction (Hide));
		} else {
			int pos = 1;
			loading.SetActive (false);
			JSONObject data = new JSONObject (result);
			JSONObject results = (JSONObject)data.GetField ("results");
			for (int i = 0; i < results.list.Count; i++) {
				JSONObject leader = (JSONObject)results.list [i];
				JSONObject user = (JSONObject)leader.GetField ("user");
				int leaderScore = (int)leader.GetField ("value").n;
				string nickname = user.GetField ("nickname").str;
				Vector3 position = new Vector3 (0, 0, 0);
				RectTransform newPlayer = Instantiate (playerPrefab.transform, position, transform.rotation) as RectTransform;
				newPlayer.GetComponent<PlayerRank> ().SetScore (type, pos, nickname, leaderScore);
				newPlayer.SetParent (players.transform);
				newPlayer.localScale = new Vector3 (1, 1, 1);
				pos++;
			}
		}
	}

	void Hide(){
		busy = false;
		gameObject.SetActive (false);
		Clear ();
	}

	void Clear(){
		foreach (Transform player in players.transform) {
			Destroy(player.gameObject);
		}
	}
}
