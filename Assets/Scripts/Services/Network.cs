﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;

public class Network : MonoBehaviour {

	public DialogModal DM;
	private GameObject game;
	public byte maxPlayers;
	public bool connected;


	private Backend backend;

	// singleon
	private static Network instance;
	public static Network Instance{
		get {
			if(Network.instance == null){
				instance =  GameObject.FindObjectOfType<Network>();
				DontDestroyOnLoad(instance.gameObject);
			}
			return Network.instance;
		}
	}

	void Awake () {	
		if(instance == null){
			instance = this;
			DontDestroyOnLoad(this);
		}
		else{
			if(this != instance)
				Destroy(this.gameObject);
		}

		maxPlayers = 2;
		connected = false;
		backend = Backend.Instance;
	}

	void OnConnectedToPhoton(){
		connected = true;
		LinkPlayer ();
	}

	public void Disconnect(){
		connected = false;
		PhotonNetwork.Disconnect ();
	}

	void OnDisconnectedFromPhoton(){
		if (connected) {
			DialogModal.Instance.Show ("CONNECTION ERROR", "sorry we lost you, check your internet connection and try again", new UnityAction (OnErrorConnection));
		}
	}
	
	void OnErrorConnection(){
		Application.LoadLevel ("Main");
	}

	// link google play account with photon
	public void LinkPlayer(){
		ExitGames.Client.Photon.Hashtable setPlayerName = new ExitGames.Client.Photon.Hashtable() {
			{"username", backend.nickname},
		};
		PhotonNetwork.player.SetCustomProperties (setPlayerName);
	}
}
