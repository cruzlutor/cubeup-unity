﻿using SimpleJSON;
using UnityEngine;
using UnityEngine.Events;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class Backend : MonoBehaviour {

	
	private string APP_ID = "pVezIHhEYYVbwBFVicOITXvqUAHxtZeCNqWRZrU1";
	private string REST_KEY = "9tnHcGjsHHsvETdjaIlozH5Jpl0RihQ7iG5Okc8T";

	private Connect connect;
	public string userId;
	public string nickname;
	public string sesstionToken;
	public string SesstionToken {
		get {return sesstionToken;}
		set {sesstionToken = value; }
	}


	// singleton
	private static Backend instance;
	public static Backend Instance{
		get {
			if(Backend.instance == null){
				instance =  GameObject.FindObjectOfType<Backend>();
				DontDestroyOnLoad(instance.gameObject);
			}
			return Backend.instance;
		}
	}
	
	
	// Use this for initialization
	void Awake () {
		if(instance == null){
			instance = this;
			DontDestroyOnLoad(this);
		}
		else{
			if(this != instance)
				Destroy(this.gameObject);
		}

		nickname = "GUEST_" + Random.Range (00000, 99999);
		connect = Connect.Instance;
	}
	
	Dictionary<string, string> Headers(){
		Dictionary<string, string> headers = new Dictionary<string, string> ();
		headers.Add("X-Parse-Application-Id", APP_ID);
		headers.Add("X-Parse-REST-API-Key", REST_KEY);
		headers.Add("X-Parse-Revocable-Session", "1");
		headers.Add("Content-Type", "application/json");

		if (sesstionToken != null) {
			headers.Add("X-Parse-Session-Token", sesstionToken);
		}

		return headers;
	}

	public WWW GET(string url){
		Dictionary<string, string> headers = Headers ();
		return new WWW(url, null, headers);
	}

	public WWW POST(string url, string data){
		Dictionary<string, string> headers = Headers ();
		return new WWW(url, Encoding.UTF8.GetBytes(data), headers );
	}

	IEnumerator WaitForRequest(WWW www, UnityAction<bool, string> callback){
		yield return www;
		if (www.error == null) {
			callback (true, www.data);
		} else {
			callback (false, www.error);
		}
	}

	public void LogIn(string username, string password, UnityAction<bool, string> callback){
		WWW www = GET ("https://api.parse.com/1/login?username=" + username + "&password=" + password);
		StartCoroutine(WaitForRequest(www, callback));
	}

	public void SignUp(string username, string password, string nickname, UnityAction<bool, string> callback){
		JSONObject data = new JSONObject(JSONObject.Type.OBJECT);
		data.AddField ("username", username);
		data.AddField ("password", password);
		data.AddField ("nickname", nickname);
		WWW www = POST ("https://api.parse.com/1/users", data.ToString());
		StartCoroutine(WaitForRequest(www, callback));
	}

	public void CreateMatch(string roomName, UnityAction<bool, string> callback){
		JSONObject data = new JSONObject(JSONObject.Type.OBJECT);
		data.AddField ("active", true);
		data.AddField ("roomName", roomName);

		if (connect.connected == true) {
			data.AddField("user", new JSONObject (delegate(JSONObject UserPointer) {
				UserPointer.AddField ("__type", "Pointer");
				UserPointer.AddField ("className", "_User");
				UserPointer.AddField ("objectId", userId);
			}));

			data.AddField("ACL", new JSONObject (delegate(JSONObject ACLWrapper) {
				ACLWrapper.AddField(userId, new JSONObject (delegate(JSONObject ACLUser) {
					ACLUser.AddField ("read", true);
					ACLUser.AddField ("write", true);
				}));

				ACLWrapper.AddField("*", new JSONObject (delegate(JSONObject ACLUser) {
					ACLUser.AddField ("read", true);
				}));
			}));
		}

		WWW www = POST("https://api.parse.com/1/classes/Match", data.ToString());
		StartCoroutine(WaitForRequest(www, callback));
	}

	public void EndMatch(string matchId, int blocks, int accurate, int perfect, int position, UnityAction<bool, string> callback){
		if (connect.connected == true) {
			JSONObject data = new JSONObject (JSONObject.Type.OBJECT);
			data.AddField ("matchId", matchId);
			data.AddField ("b", blocks);
			data.AddField ("a", accurate);
			data.AddField ("pe", perfect);
			data.AddField ("po", position);
			WWW www = POST ("https://api.parse.com/1/functions/endMatch", data.ToString ());
			StartCoroutine (WaitForRequest (www, callback));
		} else {
			callback(false, "{}");
		}
	}

	public void RecoverMatch(string matchId, UnityAction<bool, string> callback){
		JSONObject data = new JSONObject(JSONObject.Type.OBJECT);
		data.AddField ("matchId", matchId);
		WWW www = POST("https://api.parse.com/1/functions/recoverMatch", data.ToString());
		StartCoroutine(WaitForRequest(www, callback));
	}
	
	public void GetLeaders(int type, UnityAction<bool, string> callback){

		string filter = "cube";

		if (type == 2) 
			filter = "exp";

		if (type == 3)
			filter = "accurate";

		WWW www = GET ("https://api.parse.com/1/classes/Score/?where={\"type\":\""+filter+"\"}&order=-value&limit=50&include=user");
		StartCoroutine (WaitForRequest (www, callback));
	}
}
