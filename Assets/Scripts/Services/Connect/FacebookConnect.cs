﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using Facebook;
//using Facebook.Unity;

public class FacebookConnect : MonoBehaviour, IConnect {

	private string userId;
	private UnityAction onInitCB;
	private UnityAction<bool> onLoginCB;

	private void OnInitComplete()
	{
		onInitCB ();
	}
	
	private void OnHideUnity(bool isGameShown)
	{
		if (!isGameShown) {
			// pause the game - we will need to hide
			Time.timeScale = 0;
		} else {
			// start the game back up - we're getting focus again
			Time.timeScale = 1;
		}
	}

	public void Init(UnityAction callback){
		onInitCB = callback;
		if (FB.IsInitialized) {
			onInitCB();
		} else {
			FB.Init (OnInitComplete, OnHideUnity);
		}
	}
	
	public void LogIn( UnityAction<bool> callback){
		onLoginCB = callback;
		FB.LogInWithReadPermissions ("public_profile,email", OnLogin);
	}

	public void OnLogin(FBResult result){
		if (FB.IsLoggedIn) {
			onLoginCB (true);
		} else {
			onLoginCB (false);
		}
	}


	public void LogOut(){
		
	}
	
	public string GetId(){
		if (FB.IsLoggedIn) {
			return FB.UserId;
		} else {
			return Backend.Instance.userId;
		}
	}
}