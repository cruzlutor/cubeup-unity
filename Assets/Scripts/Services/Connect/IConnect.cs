﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;

public interface IConnect{
	
	void Init (UnityAction callback);
	
	void LogIn (UnityAction<bool> callback);
	
	void LogOut ();

	string GetId();

	//string GetUsername();
}
