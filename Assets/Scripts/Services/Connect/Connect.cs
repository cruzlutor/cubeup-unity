﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Connect : MonoBehaviour {

	public bool first;
	public bool connected;
	public IConnect service;
	private string username;
	private Backend backend;
	private UnityAction<bool> logincb;
	private UnityAction loginfcb;

	// singleton
	private static Connect instance;
	public static Connect Instance{
		get {
			if(Connect.instance == null){
				instance =  GameObject.FindObjectOfType<Connect>();
				DontDestroyOnLoad(instance.gameObject);
			}
			return Connect.instance;
		}
	}

	// Use this for initialization
	void Awake () {

		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (this);
		} else {
			if (this != instance)
				Destroy (this.gameObject);
		}

		first = true;
		backend = Backend.Instance;
		connected = false;
		username = "guest_" + Random.Range (00000, 99999);
	}

	// android connect controller
	public void Android(UnityAction callback){
		service = gameObject.GetComponent<GooglePlayConnect> ();
		service.Init (callback);
	}

	// facebook connect controller
	public void Facebook(UnityAction callback){
		service = gameObject.GetComponent<FacebookConnect> ();
		service.Init (callback);
	}
	
	// facebook connect controller
	public void IOS(UnityAction callback){
		service = gameObject.GetComponent<GameCenterConnect> ();
		service.Init (callback);
	}
/*
	void OnBackendSignUp(bool success, string result){
		if (success) {
			JSONObject data = new JSONObject(result);
			backend.userId = data.GetField("objectId").str;
			backend.sesstionToken = data.GetField("sessionToken").str;
		}
		connected = success;
		logincb(success);
	}
*/
	void OnBackendLogin(bool success, string result){
		if (success) {
			connected = success;
			JSONObject data = new JSONObject(result);
			backend.userId = data.GetField("objectId").str;
			backend.sesstionToken = data.GetField("sessionToken").str;
			backend.nickname = data.GetField("nickname").str;

			print (backend.nickname);
			logincb(true);
		} else {
			loginfcb();
		}
	}

	public void BackendSignup(string nickname, UnityAction<bool, string> callback){
		string username = GetId();
		string password = username.Substring(username.Length - 3);
		backend.SignUp(username, password, nickname, callback);
	}

	// login function
	public void LogIn( UnityAction<bool> callback, UnityAction callbackFail){
		logincb = callback;
		loginfcb = callbackFail;
		first = false;
		service.LogIn ((bool success) => {
			if(success){
				string username = GetId();
				string password = username.Substring(username.Length - 3);
				backend.LogIn(username, password, OnBackendLogin);
			}else{
				connected = false;
				logincb(false);
			}
		});
	}

	// logout function
	public void Logout(){
		connected = false;
		service.LogOut ();
	}

	// get connect user id
	public string GetId(){
		return service.GetId ();
	}
}
