﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class GooglePlayConnect : MonoBehaviour, IConnect {

	public void Init(UnityAction callback){
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate ();
		callback ();
	}

	public void LogIn( UnityAction<bool> callback){
		Social.localUser.Authenticate((bool success) => {
			callback (success);
		});
	}
	
	public void LogOut(){
		PlayGamesPlatform.Instance.SignOut();
	}

	public string GetId(){
		return Social.localUser.id;
	}
}
