﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.SocialPlatforms;
public class GameCenterConnect : MonoBehaviour, IConnect {

	public void Init(UnityAction callback){
		callback ();
	}
	
	public void LogIn( UnityAction<bool> callback){
		Social.localUser.Authenticate((bool success) => {
			callback (success);
		});
	}
	
	public void LogOut(){
		//
	}
	
	public string GetId(){
		return Social.localUser.id;
	}
}
