﻿using UnityEngine;
using System.Collections;

public class Cube : MonoBehaviour {

    private Cubeup cubeup;

    void Awake()
    {
        cubeup = Cubeup.Instance;
    }

	// Use this for initialization
	void Start () {
        Init();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void Init()
    {
        //ChangeColor();
    }

    public void MultiColor()
    {

    }

    public void ChangeColor(Color color)
    {
        GetComponent<SpriteRenderer>().color = color;
    }
}
