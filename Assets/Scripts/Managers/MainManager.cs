﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
//using Parse;
//using Facebook.Unity;
using SimpleJSON;

//using Facebook;
public class MainManager : MonoBehaviour {

	private SettingsModal settingsModal;
	private Leaderboard leaderboard;
	private Backend backend;
	private Connect connect;
	public GameObject mainMenu;
	public Text nickname;
	public string text;

	void OnGUI() {
		GUIStyle guiStyle = new GUIStyle ();
		guiStyle.fontSize = 30;
		GUI.Label (new Rect (10, 10, 100, 20), text, guiStyle);
	}
	
	void Awake(){
		connect = Connect.Instance;
		backend = Backend.Instance;
		leaderboard = Leaderboard.Instance;
		settingsModal = SettingsModal.Instance;
    }

	// setup the connect controller
	void Start () {
		if (PlayerPrefs.GetInt ("sound", 1) == 0) {
			AudioListener.volume = 0;
		}
		QualitySettings.vSyncCount = 0;
		Application.targetFrameRate = 45;

		//backend.LogIn("aaaaaa", "aaaaaa", OnBackendLogin);
		/*
		ParseUser.LogInAsync ("aaaaaa", "bbbbbb").ContinueWith (t => {
			if (t.IsFaulted || t.IsCanceled){


				ParseUser user = new ParseUser ();
				user ["username"] = "aaaaaa";
				user ["password"] = "bbbbbb";
				
				
				ParseUser.LogOutAsync().ContinueWith(c=>{
					user.SignUpAsync().ContinueWith(b =>{
						if (b.IsFaulted || b.IsCanceled){
							print ("signup fail");
						}else{
							print ("signup success");
						}
					});
				});
				

				mainMenu.GetComponent<MainMenu> ().Nickname ();
				
			}else{
				print ("login success");
			}
		});
		*/

		//mainMenu.GetComponent<MainMenu> ().GuestMenu ();




		//InitConnect ();

		if (connect.first == true) {
			InitConnect ();
		} else {
			OnLogIn(connect.connected);
		}

		/*
		Social.localUser.Authenticate( (bool success) => {
			if (success) {
				Debug.Log ("Authentication successful");
				string userInfo  = "Username: " + Social.localUser.userName + 
					"\nUser ID: " + Social.localUser.id + 
						"\nIsUnderage: " + Social.localUser.underage;
				Debug.Log (userInfo);   
				
			}
			else
				Debug.Log ("Authentication failed");
		});*/
	}


	void OnBackendLogin(bool success, string result){

		if (!success) {
			mainMenu.GetComponent<MainMenu> ().Nickname ();
		} else {
			OnLogIn(true);
		}

	}

	public void InitConnect(){
		if (Application.platform == RuntimePlatform.Android) {
			StartCoroutine (WaitConnection ());
			connect.Android (OnInitAndroid);

		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
			StartCoroutine (WaitConnection ());
			connect.IOS (OnInitIOS);

		} else if (Application.platform == RuntimePlatform.WebGLPlayer) {
			StartCoroutine (WaitConnection ());
			connect.Facebook (OnInitFacebook);

		} else if (Application.platform == RuntimePlatform.WindowsEditor) {
			connect.Facebook (OnInitFacebook);
			
		} else {
			OnLogIn(false);
		}
	}

	IEnumerator WaitConnection(){
		mainMenu.GetComponent<MainMenu> ().Loader ();
		yield return new WaitForSeconds(10.0f);
		if (mainMenu.GetComponent<MainMenu> ().status == "loader") {
			mainMenu.GetComponent<MainMenu> ().Reconnect ();
		}
	}

	void OnInitAndroid(){
		LogIn ();
	}

	void OnInitIOS(){
		LogIn ();
	}

	void OnInitFacebook(){
		mainMenu.GetComponent<MainMenu> ().GuestMenu ();
	}

	public void LogIn(){
		mainMenu.GetComponent<MainMenu> ().Loader ();
		connect.LogIn (OnLogIn, OnLogInFail);
	}

	void OnLogIn(bool status){
		if (mainMenu.GetComponent<MainMenu> ().status == "reconnect" || mainMenu.GetComponent<MainMenu> ().status == "loader") {
			if (!status) {
				mainMenu.GetComponent<MainMenu> ().GuestMenu ();
			} else {
				mainMenu.GetComponent<MainMenu> ().UserMenu ();
			}
		}
	}

	void OnLogInFail(){
		mainMenu.GetComponent<MainMenu> ().Nickname ();
	}


	public void OnEditName(string nickname){
		mainMenu.GetComponent<MainMenu>().error.SetActive(false);
	}

	public void SignUp(){
		mainMenu.GetComponent<MainMenu> ().Loader ();
		connect.BackendSignup (nickname.text, OnSignUp);
	}

	void OnSignUp(bool success, string result){
		if (success) {
			JSONObject data = new JSONObject (result);
			backend.nickname = nickname.text;
			backend.userId = data.GetField ("objectId").str;
			backend.sesstionToken = data.GetField ("sessionToken").str;
			connect.connected = true;
			Play();
		} else {
			mainMenu.GetComponent<MainMenu> ().Nickname ();
			mainMenu.GetComponent<MainMenu>().error.SetActive(true);
		}
	}

	// play match action
	public void Play(){
        Application.LoadLevel("Game");
        //Application.LoadLevel("Lobby");
        /*
        if (Cubeup.Instance.ShowTutorial())
        {
            tutorial.SetActive(true);
        }
        else
        {
            //tutorial.SetActive(false);
            Application.LoadLevel("Lobby");
        }
        */
    }

	public void ShowLeaderBoard(){
		leaderboard.ShowScore ();
	}	
	
	public void ShowSettings(){
		settingsModal.Show ();
	}	
}