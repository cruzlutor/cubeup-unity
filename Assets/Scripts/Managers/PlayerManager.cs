﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {

    private GameObject playerPrefab;
    private GameObject me;
    private GameObject enemy;
    private Cubeup cubeup;

    void Awake()
    {
        playerPrefab = Resources.Load("Prefabs/Player") as GameObject;
        cubeup = Cubeup.Instance;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

    }

    public void Show()
    {
        gameObject.SetActive(true);
        Init();
    }

    void Init()
    {
        InitPlayers();
    }

    void InitPlayers()
    {
        me = AddPlayer(true);
        enemy = AddPlayer(false);
    }

    GameObject AddPlayer(bool client)
    {
        Vector3 position = new Vector3(0f, 0f, 0f);
        RectTransform newPlayer = Instantiate(playerPrefab.transform, position, transform.rotation) as RectTransform;
        newPlayer.parent = transform;
        newPlayer.localScale = new Vector3(1, 1, 1);
        newPlayer.GetComponent<PlayerHUD>().isClient = client;
        newPlayer.GetComponent<PlayerHUD>().Init();
        return newPlayer.gameObject;
    }

    public void AssignTurn(bool clientTurn)
    {
        if (clientTurn)
        {
            me.GetComponent<PlayerHUD>().ActiveTurn();
            enemy.GetComponent<PlayerHUD>().PuaseTurn();
        }
        else
        {
            me.GetComponent<PlayerHUD>().PuaseTurn();
            enemy.GetComponent<PlayerHUD>().ActiveTurn();
        }
    }

    public string ClientColor()
    {
        string [,] color = me.GetComponent<PlayerHUD>().currentColor;
        return color[0, 0];
    }
}
