﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public ColorManager colors;
    public GameObject tower;
    public GameObject players;
    public GameObject score;
    public GameObject status;
    public GameObject endMenu;

    private Backend backend;
    private Network network;
    private Cubeup cubeup;

    private float countdown;
    private bool clientTurn;
    public bool playing;
    public int cubes;

    private PhotonView photonView;

    void Awake()
    {
        colors = new ColorManager();

        tower.SetActive(false);
        players.SetActive(false);
        score.SetActive(false);
        endMenu.SetActive(false);

        backend = Backend.Instance;
        network = Network.Instance;
        cubeup = Cubeup.Instance;

        photonView = PhotonView.Get(this);

        cubes = 0;

        playing = false;
    }

	// Use this for initialization
	void Start ()
    {
        Connect();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(playing && clientTurn)
            {
                PickColor();
            }
        }
    }

    public void Retry()
    {
        Application.LoadLevel("Game");
    }

    // init photon connection
    void Connect()
    {
        status.GetComponent<StatusHUD>().SetStatus("connecting...");
        PhotonNetwork.ConnectUsingSettings("v1.0");
    }

    // on connect to photon
    void OnConnectedToPhoton()
    {
        status.GetComponent<StatusHUD>().SetStatus("looking game...");
    }

    // auto join lobby
    void OnJoinedLobby()
    {
        JoinRoom();
    }

    // try to join in a random room
    void JoinRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    // if random join fail
    void OnPhotonRandomJoinFailed()
    {
        string roomName = "" + Random.Range(000000, 999999) + "" + Time.time.ToString();
        RoomOptions roomOptions = new RoomOptions() { isVisible = true, maxPlayers = network.maxPlayers };
        PhotonNetwork.CreateRoom(roomName, roomOptions, TypedLobby.Default);
    }

    // on success room connected
    void OnJoinedRoom()
    {
        OnPhotonPlayerConnected();
    }

    // on player connected to a room
    void OnPhotonPlayerConnected()
    {
        CheckRoom();
    }

    void CheckRoom()
    {
        if (PhotonNetwork.room.playerCount == network.maxPlayers)
        {
            PhotonNetwork.room.open = false;
            StartCount();
        }
    }

    void StartCount()
    {
        Countdown.Init(3, status.GetComponent<StatusHUD>().statusText, StartGame);

        ShowScore();
        ShowPlayers();
        ShowTower();
    }

    void ShowScore()
    {
        score.GetComponent<ScoreHUD>().Show();
    }

    void ShowPlayers()
    {
        players.GetComponent<PlayerManager>().Show();
    }

    void ShowTower()
    {
        tower.GetComponent<TowerManager>().Show();
    }

    void ShowEndMenu()
    {
        endMenu.GetComponent<EndMenu>().Show();
    }

    void StartGame()
    {
        playing = true;
        ResolveTurn();
        AssignTurn();
    }

    void ResolveTurn()
    {
        clientTurn = (PhotonNetwork.room.masterClientId == PhotonNetwork.player.ID);
    }

    void ChangeTurn()
    {
        clientTurn = (clientTurn == false ? true : false );
        AssignTurn();
    }

    void AssignTurn()
    {

        // change the current color
        colors.NextColor();

        // assign new turn
        players.GetComponent<PlayerManager>().AssignTurn(clientTurn);

        // change turn status text 
        status.GetComponent<StatusHUD>().SetStatus((clientTurn == true ? "your turn" : "enemy turn"));
        
        // if is the client turns, active the multicolor
        if (clientTurn)
        {
            tower.GetComponent<TowerManager>().AddCube(true);
            tower.GetComponent<TowerManager>().Multicolor(true);
        }
        // if is the enemy turns, deactivate the multicolor
        else
        {
            tower.GetComponent<TowerManager>().Multicolor(false);
        }

        score.GetComponent<ScoreHUD>().SetScore(cubes);
    }

    void EnemyCube()
    {
        tower.GetComponent<TowerManager>().AddCube(false);
        score.GetComponent<ScoreHUD>().SetScore(cubes);
    }

    void PickColor()
    {
        CheckColor();
    }

    void CheckColor()
    {
        if( tower.GetComponent<TowerManager>().colors.name == colors.name)
        {
            NextTurn();
        }
        else
        {
            Lose();
        }   
    }

    void NextTurn()
    {
        cubes++;
        ChangeTurn();
        photonView.RPC("OnNextTurn", PhotonTargets.Others, PhotonNetwork.player);
    }

    void Win()
    {
        status.GetComponent<StatusHUD>().SetStatus("you win");
        Invoke("EndGame", 1f);
    }

    void Lose()
    {
        status.GetComponent<StatusHUD>().SetStatus("you lose");
        photonView.RPC("OnLose", PhotonTargets.Others, PhotonNetwork.player);
        Invoke("EndGame", 1f);
    }

    void EndGame()
    {
        playing = false;
        tower.GetComponent<TowerManager>().Multicolor(false);
        network.Disconnect();
        ShowEndMenu();
        
    }

    [PunRPC]
    void OnNextTurn(PhotonPlayer sender)
    {
        cubes++;
        EnemyCube();
        Invoke("ChangeTurn", 1);
    }

    [PunRPC]
    void OnLose(PhotonPlayer sender)
    {
        Win();
    }
}
