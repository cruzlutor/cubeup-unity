﻿using UnityEngine;
using System.Collections;

public class TowerManager : MonoBehaviour {

    public GameObject cubePrefab;
    public ColorManager colors;
    private GameObject gameManager;

    public bool multicolor;
    private float timing;
    private float timingLimit;

    // Use this for initialization
    void Awake () {
        colors = new ColorManager();
        timingLimit = 1f;
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
    }
	
	// Update is called once per frame
	void Update () {
        
        if (multicolor == true)
        {
            timing += Time.deltaTime;
            if(timing >= timingLimit)
            {
                timing = 0f;
                ChangeColor();
            }
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Multicolor(bool enabled)
    {
        multicolor = enabled;
        timing = 0f;
    }

    void ChangeColor()
    {
        colors.NextColor();
        transform.GetChild((transform.childCount-1)).gameObject.GetComponent<Cube>().ChangeColor(colors.color);
    }

    public void AddCube(bool client)
    {
        // previus block
        GameObject previus = transform.GetChild(transform.childCount - 1).gameObject;

        // get previous render element to get the w/h
        Renderer renderer = previus.GetComponent<Renderer>();

        // get new block position
        Vector2 position = new Vector2(previus.transform.position.x, previus.transform.position.y + renderer.bounds.size.y);

        Transform newCube = Instantiate(cubePrefab.transform, position, transform.rotation) as Transform;
        newCube.parent = transform;

        colors.NextColor();


        if (client)
        {
            // change color to the local
            ChangeColor();
        }
        else
        {
            // change the color to the global
            newCube.gameObject.GetComponent<Cube>().ChangeColor(gameManager.GetComponent<GameManager>().colors.color);
        }

        /* make scroll */
        if (transform.childCount > 3)
        {
            gameObject.GetComponent<TowerScroll>().Move(renderer.bounds.size.y);
        }

    }

}
