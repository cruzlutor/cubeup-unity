﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatusHUD : MonoBehaviour {

    public Text statusText;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

    }

    public void SetStatus(string _text)
    {
        statusText.text = _text;
    }

}
