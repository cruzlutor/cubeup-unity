﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHUD : MonoBehaviour {

    public bool isTurn;
    public bool isClient;
    public PhotonPlayer playerNetwork;
    public Image playerColor;
    public Text playerName;
    public Text colorName;
    public Text colorLabel;
    public string[,] currentColor;

    private Cubeup cubeup;
    private GameObject gameManager;

    void Awake()
    {
        cubeup = Cubeup.Instance;
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

    }

    public void Init()
    {

    }

    public void PuaseTurn()
    {
        isTurn = false;
        Color color;
        ColorUtility.TryParseHtmlString("#F2F2F2", out color);
        playerName.color = Color.gray;
        colorLabel.text = "";
        SetColor(color, "");
    }

    public void ActiveTurn()
    {
        isTurn = true;
        ColorManager colors = gameManager.GetComponent<GameManager>().colors;
        playerName.color = Color.white;
        colorLabel.text = "color";
        SetColor(colors.color, colors.name);
    }

    public void SetColor(Color color, string name)
    {
        playerColor.color = color;
        colorName.color = color;
        colorName.text = name;
    }
}
