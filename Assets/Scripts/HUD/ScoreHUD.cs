﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreHUD : MonoBehaviour {

    public Text cubes;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void SetScore(int score)
    {
        cubes.text = ""+score;
    }
}
