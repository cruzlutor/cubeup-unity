var ScoreModel = Parse.Object.extend("Score");

Parse.Cloud.beforeSave("_User", function(request, response){

    query = new Parse.Query("User");
    query.equalTo("nickname", request.object.get("nickname"));

    query.find({
        success: function(user){
            if(!user[0]){
                response.success();
            }else{
                response.error("the user already exists");
            }
        },
        error: function(result){
            response.error("Not found");
        }
    });
});


/*
Parse.Cloud.define("signUp", function(request, response){

    query = new Parse.Query("User");
    query.equalTo("nickname", request.params.nickname);

    query.find({
        success: function(user){
            if(!user[0]){
                var user = new Parse.User();
                user.set("username", request.params.username);
                user.set("password", request.params.password);
                user.set("nickname", request.params.nickname);

                user.signUp(null, {
                    success: function(result){
                        Parse.User.logIn(request.params.username, request.params.password, {
                          success: function(login) {
                              login.add("hola", "chao")
                            response.success(login);
                          },
                          error: function(login, error) {
                            response.error(result);
                          }
                        });
                    },
                    error: function(result){
                        response.error(result);
                    }
                });
            }else{
                response.error("the user already exists");
            }
        },
        error: function(result){
            response.error("Not found");
        }
    });
});*/

Parse.Cloud.define("recoverMatch", function(request, response){
    var user = request.user;

    query = new Parse.Query("Match");
    query.get(request.params.matchId);
    query.find({
        success: function(match){

            var acl = new Parse.ACL(user);
            acl.setPublicReadAccess(true);

            match[0].set('user', user);
            match[0].set('ACL', acl);
            match[0].save();
            response.success("ok");
        },
        error: function(result){
            response.error("Not found");
        }
    });

});

//set the scores and end match time
Parse.Cloud.define("endMatch", function(request, response){
    Parse.Cloud.useMasterKey();
    var user = request.user;

    if(!user){
        console.log("a");
        return response.error("user not found");
    }
    //response.success(user.get("username"));

    query = new Parse.Query("Match");
    query.get(request.params.matchId);
    /*
    query.equalTo("roomName", request.params.roomName);
    query.equalTo("active", true);
    */
    query.find({
        success: function(match){
            if(match[0]){

                if(match[0].get('active') == false){
                    return response.error("is not active");
                }

                var matchStats = {
                    'blocks' : request.params.b,
                    'exp' : request.params.b,
                    'accurate' : request.params.a,
                    'perfect' : request.params.pe,
                    'position' : request.params.po,
                }

                // save new data
                match[0].set("stats", matchStats);
                match[0].set("endAt", new Date());
                match[0].set("active", false);
                match[0].save();

                // update score

                query = new Parse.Query("Score");
                query.equalTo("user", user);
                //query.equalTo("type", "exp");
                query.find({
                    success: function(results){
                        var scoreExp = null;
                        var scoreCube = null;
                        var scoreAccurate = null;

                        // create if not exists
                        if(!results[0]){
                            var acl = new Parse.ACL();
                            acl.setPublicReadAccess(true);
                            acl.setPublicWriteAccess(false);

                            scoreExp = new ScoreModel();
                            scoreExp.set("user", user);
                            scoreExp.set("type", "exp");
                            scoreExp.set("value", 0);
                            scoreExp.set("ACL", acl);

                            scoreCube = new ScoreModel();
                            scoreCube.set("user", user);
                            scoreCube.set("type", "cube");
                            scoreCube.set("value", 0);
                            scoreCube.set("ACL", acl);

                            scoreAccurate = new ScoreModel();
                            scoreAccurate.set("user", user);
                            scoreAccurate.set("type", "accurate");
                            scoreAccurate.set("value", 0);
                            scoreAccurate.set("ACL", acl);

                        }else{

                            for(result in results){
                                if(results[result].get("type") == "exp"){
                                    scoreExp = results[result];
                                }
                                if(results[result].get("type") == "cube"){
                                    scoreCube = results[result];
                                }
                                if(results[result].get("type") == "accurate"){
                                    scoreAccurate = results[result];
                                }
                            }
                        }


                        // check score hacks
                        var dateDif = match[0].get("endAt") - match[0].createdAt;
                        var seconds = Math.abs(dateDif / 1000);

                        if(matchStats["exp"] >= seconds){
                            return response.error("bad data");
                        }

                        if(matchStats["accurate"] > matchStats["exp"]){
                            return response.error("bad data");
                        }

                        if(matchStats["perfect"] > matchStats["exp"]){
                            return response.error("bad data");
                        }

                        // save exp score
                        var previousExp = scoreExp.get("value");
                        var gainedExp =  matchStats["exp"] + matchStats["accurate"] + (matchStats["perfect"]*2);

                        if(matchStats["position"] == 1){
                            gainedExp = gainedExp*2;
                        }
                        if(matchStats["position"] == 3){
                            gainedExp = 0;
                        }

                        var currentExp = previousExp + gainedExp;

                        scoreExp.set("value", currentExp);


                        // save cube score
                        var previousCube = scoreCube.get("value");
                        if(matchStats["blocks"] >= previousCube){
                            scoreCube.set("value", matchStats["blocks"]);
                        }

                        // save accurate score
                        var previousAccurate = scoreAccurate.get("value");
                        if(matchStats["accurate"] >= previousAccurate){
                            scoreAccurate.set("value", matchStats["accurate"] + matchStats["perfect"]);
                        }

                        scoreExp.save();
                        scoreCube.save();
                        scoreAccurate.save();
                        console.log("e");
                        response.success({previousExp:previousExp, currentExp:currentExp});
                    },
                    error: function(results){
                        console.log("b");
                        response.error("error search");
                    },

                });
            }else{
                console.log("c");
                response.success("result 0 not found");
            }



        },
        error: function(error){
            console.log("d");
            response.error("Not found");
        }
    });

});
                /*
                // calculate the exp
                matchStats["exp"] = matchStats["blocks"];

                // save new data
                results[0].set("stats", matchStats);
                results[0].set("active", false);
                results[0].save();

                // update score

                query = new Parse.Query("Score");
                query.equalTo("user", user);
                query.equalTo("type", "exp");
                query.find({
                    success: function(results){
                        var score = null;
                        // create if not exists
                        if(!results[0]){
                            var acl = new Parse.ACL(user);
                            acl.setPublicReadAccess(true);

                            score = new ScoreModel();
                            score.set("user", user);
                            score.set("type", "exp");
                            score.set("value", 0);
                            score.set("ACL", acl);
                        }else{
                            score = results[0];
                        }

                        score.set("value", score.get("value") + matchStats["exp"]);
                        score.save();

                        response.success("ok");
                    },
                    error: function(results){
                        response.error("no");
                    },

                });
                */




//calculate the position and player exp
Parse.Cloud.define("saveMatch", function(request, response){
    var user = request.user;

    query = new Parse.Query("Match");
    query.equalTo("user", user);
    query.equalTo("roomName", request.params.roomName);
    query.equalTo("active", true);

    query.find({
        success: function(results){
            if(results[0]){

                // get the stats
                var matchStats = results[0].get("stats");

                // calculate the exp
                matchStats["exp"] = matchStats["blocks"];

                // save new data
                results[0].set("stats", matchStats);
                results[0].set("active", false);
                results[0].save();

                // update score

                query = new Parse.Query("Score");
                query.equalTo("user", user);
                query.equalTo("type", "exp");
                query.find({
                    success: function(results){
                        var score = null;
                        // create if not exists
                        if(!results[0]){
                            var acl = new Parse.ACL(user);
                            acl.setPublicReadAccess(true);

                            score = new ScoreModel();
                            score.set("user", user);
                            score.set("type", "exp");
                            score.set("value", 0);
                            score.set("ACL", acl);
                        }else{
                            score = results[0];
                        }

                        score.set("value", score.get("value") + matchStats["exp"]);
                        score.save();

                        response.success("ok");
                    },
                    error: function(results){
                        response.error("no");
                    },
                });

            }else{
                response.success("no");
            }

        },
        error: function(error){
            response.error("Not found");
        }
    });
});

//get leader board
Parse.Cloud.define("leaderboard", function(request, response){
    response.success("ok");
    /*
    query = new Parse.Query("Score");
    query.equalTo("type", "exp");
    //query.descending("value");
    query.find({
        success: function(results){
            if(results[0]){
                response.success(results);
            }else{
                response.success("mal");
            }

        },
        error: function(results){
            response.error("Not found");
        }
    })*/
});
